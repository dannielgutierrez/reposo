
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class ServerFile{
    
	public static void main (String[] args) throws IOException {
	
			
		    ServerSocket server = new ServerSocket(1414);
		    
	        System.out.println("...");
	        Socket socket = server.accept();
	        System.out.println("Conexion aceptada");
	    
	        Scanner sc = new Scanner(socket.getInputStream());
	        
            FileWriter file = new FileWriter("logfle.txt");

            String thisLine;
		    while (sc.hasNext()){
		        thisLine = sc.nextLine();
		        if(thisLine.equals("EOF"))
		            break;
		        file.write(thisLine+"\n");
		        System.out.println(thisLine);
		    }
	        
	        file.close();        
	        socket.close();    
		
	} 
}
