
import java.io.*;
import java.net.*;

public class DateServer{
    
    public static void main(String[] args) throws IOException{
        
        ServerSocket socket = new ServerSocket(6013);
        
        Socket client;
        
        
        while(true){
            client = socket.accept();
            System.out.println("Conexion aceptada, IP " + client.getInetAddress() );
            
            PrintWriter pout = new PrintWriter(client.getOutputStream(),true);
            
            pout.println( new java.util.Date().toString() );
            
            
			client.close();
        }
        
        
    }    
}