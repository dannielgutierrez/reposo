
import java.io.*;
import java.net.*;

public class EjSockets{
    
    public static void main(String[] args) throws IOException{
        
        ServerSocket socket = new ServerSocket(6013);
        
        Socket client;
        
        /**
        * Use: netstat -a | grep 6013 
        * 
        * Use: telnet 52.7.42.176 6013
        *             52.6.220.57
        * 
        */
        
        while(true){
            client = socket.accept();
            
            System.out.println("Conexion aceptada, IP " + client.getInetAddress() );
            
			client.close();
        }
        
        
    }    
}
