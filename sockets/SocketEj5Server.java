
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class SocketEj5Server{
    
	public static void main (String[] args) throws IOException {
	
			
		    ServerSocket server = new ServerSocket(8001);
		    
		    while (true){
		        System.out.println("Esperando conexiones");
		    Socket socket = server.accept();
		    System.out.println("Conexion aceptada");
		    
		    Scanner sc = new Scanner(socket.getInputStream());

		    String file = sc.nextLine();

            System.out.println("Sirviendo "+file);
		    
	        File f = new File(file);

		    Scanner sc2 = new Scanner(f);
		   
		    PrintWriter sockOut = new PrintWriter(socket.getOutputStream(),true);
		    
		    while (sc2.hasNext()){
		        String thisline = sc2.nextLine();
		        sockOut.println(thisline); 
		        //System.out.println(thisline); 
		    }
		    
		    
		   
		    
		    }
		
	 //socket.close();
	
	} 
}
