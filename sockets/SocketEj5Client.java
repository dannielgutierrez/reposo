
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class SocketEj5Client{
    
    public static void main(String[] args) throws IOException{
        
        
        Scanner consoleIn = new Scanner(System.in);
        
        Socket sock = new Socket("127.0.0.1",8001);
        PrintWriter socketOut = new PrintWriter(sock.getOutputStream(), true);
        
        System.out.print("nombre del archivo a leer  : ");
        String filename = consoleIn.nextLine();
        
        socketOut.println(filename);
        
   	    Scanner sc = new Scanner( sock.getInputStream() );
        
        while(sc.hasNext())
	        System.out.println(sc.nextLine());
		    
        sock.close();
        


        
        
    }    
}
