
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class EchoServer{
    
	public static void main (String[] args) throws IOException {
	
			
		    ServerSocket server = new ServerSocket(1414);
		    
	        System.out.println("...");
	        Socket socket = server.accept();
	        System.out.println("Conexion aceptada");
	    
	        Scanner sc = new Scanner(socket.getInputStream());

            String thisLine;
		    while ((thisLine = sc.nextLine())!=null && !thisLine.equalsIgnoreCase("FIN"))
		        System.out.println(thisLine);
		        
		    System.out.println(thisLine);
	    
	        socket.close();    
		
	
	} 
}
