
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class EchoClient{
    
    public static void main(String[] args) throws IOException{
        
        
        Socket sock = new Socket("127.0.0.1",1414);
        
        System.out.println("Cliente conectado:");
        
        
        Scanner consoleIn = new Scanner(System.in);
        PrintWriter pw = new PrintWriter(sock.getOutputStream(), true);
        
        
        Boolean finish = false;
        while(!finish){
            String lineToSend = consoleIn.nextLine();    
            finish = lineToSend.equalsIgnoreCase("FIN");
            pw.println(lineToSend);
        }
        
        System.out.println("Cerrando cliente...");
        
        
    }    
}
